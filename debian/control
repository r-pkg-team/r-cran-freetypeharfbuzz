Source: r-cran-freetypeharfbuzz
Section: gnu-r
Priority: optional
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-freetypeharfbuzz
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-freetypeharfbuzz.git
Homepage: https://cran.r-project.org/package=freetypeharfbuzz
Standards-Version: 4.6.2
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-fontquiver,
               libfreetype-dev,
               libharfbuzz-dev
Testsuite: autopkgtest-pkg-r

Package: r-cran-freetypeharfbuzz
Architecture: any
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R deterministic computation of text box metrics
 Unlike other tools that dynamically link to the 'Cairo'
 stack, 'freetypeharfbuzz' is statically linked to specific
 versions of the 'FreeType' and 'harfbuzz' libraries (2.9 and 1.7.6
 respectively). This ensures deterministic computation of text box
 extents for situations where reproducible results are crucial (for
 instance unit tests of graphics).
 .
 Package 'freetypeharfbuzz’ was removed from the CRAN repository.
 The code was obtained from the archive.
